/*
 * Copyright (C) 2011-2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: /Users/lina/AndroidStudioProjects/RenderScript1/app/src/main/rs/test.rs
 */
package com.iangclifton.tutorials.renderscript;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_test extends ScriptC {
    private static final String __rs_resource_name = "test";
    // Constructor
    public  ScriptC_test(RenderScript rs) {
        this(rs,
             rs.getApplicationContext().getResources(),
             rs.getApplicationContext().getResources().getIdentifier(
                 __rs_resource_name, "raw",
                 rs.getApplicationContext().getPackageName()));
    }

    public  ScriptC_test(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_s1len = 0;
        mExportVar_s2len = 0;
    }

    private FieldPacker __rs_fp_F32;
    private FieldPacker __rs_fp_I32;
    private final static int mExportVarIdx_s1len = 0;
    private int mExportVar_s1len;
    public synchronized void set_s1len(int v) {
        setVar(mExportVarIdx_s1len, v);
        mExportVar_s1len = v;
    }

    public int get_s1len() {
        return mExportVar_s1len;
    }

    private final static int mExportVarIdx_s2len = 1;
    private int mExportVar_s2len;
    public synchronized void set_s2len(int v) {
        setVar(mExportVarIdx_s2len, v);
        mExportVar_s2len = v;
    }

    public int get_s2len() {
        return mExportVar_s2len;
    }

    private final static int mExportVarIdx_d0 = 2;
    private float[] mExportVar_d0;
    public synchronized void set_d0(float[] v) {
        mExportVar_d0 = v;
        FieldPacker fp = new FieldPacker(4);
        for (int ct1 = 0; ct1 < 1; ct1++) {
            fp.addF32(v[ct1]);
        }

        setVar(mExportVarIdx_d0, fp);
    }

    public float[] get_d0() {
        return mExportVar_d0;
    }

    private final static int mExportVarIdx_d1 = 3;
    private float[] mExportVar_d1;
    public synchronized void set_d1(float[] v) {
        mExportVar_d1 = v;
        FieldPacker fp = new FieldPacker(4);
        for (int ct1 = 0; ct1 < 1; ct1++) {
            fp.addF32(v[ct1]);
        }

        setVar(mExportVarIdx_d1, fp);
    }

    public float[] get_d1() {
        return mExportVar_d1;
    }

    private final static int mExportVarIdx_signal1 = 4;
    private Allocation mExportVar_signal1;
    public void bind_signal1(Allocation v) {
        mExportVar_signal1 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_signal1);
        else bindAllocation(v, mExportVarIdx_signal1);
    }

    public Allocation get_signal1() {
        return mExportVar_signal1;
    }

    private final static int mExportVarIdx_signal2 = 5;
    private Allocation mExportVar_signal2;
    public void bind_signal2(Allocation v) {
        mExportVar_signal2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_signal2);
        else bindAllocation(v, mExportVarIdx_signal2);
    }

    public Allocation get_signal2() {
        return mExportVar_signal2;
    }

    private final static int mExportFuncIdx_dtw = 0;
    public void invoke_dtw() {
        invoke(mExportFuncIdx_dtw);
    }

}

