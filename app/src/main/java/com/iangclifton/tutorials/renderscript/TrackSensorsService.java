package com.iangclifton.tutorials.renderscript;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVWriter;


public class TrackSensorsService extends Service {

    private SensorManager mSensorManager;

    private Sensor mAccelerSensor;
    private Sensor mGravitySensor;
    private Sensor mGyroSensor;
    private Sensor mLinearAccelerSensor;
    private Sensor mMagneticSensor;
    private Sensor mRotationSensor;
    private Sensor mUncalibratedMagneticSensor;

    private CSVWriter mWriter;

    File mFileToRecord;

    String sensorType;
    String axisX;
    String axisY;
    String axisZ;

    String radioGroupText1;
    String radioGroupText2;
    String radioGroupText3;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, getResources().getString(R.string.service_started), Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ArrayList<String> optionsList = intent.getStringArrayListExtra("Options");
        if (optionsList != null) {
            radioGroupText1 = optionsList.get(0);
            radioGroupText2 = optionsList.get(1);
            radioGroupText3 = optionsList.get(2);
        }
        setUpSensors();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mWriter != null) {
            try {
                mWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mListener);
        }

        Toast.makeText(this, getResources().getString(R.string.service_stopped), Toast.LENGTH_LONG).show();
    }

    private final SensorEventListener mListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            switch (event.sensor.getType()){
                case Sensor.TYPE_ACCELEROMETER:
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_GRAVITY:
                    sensorType = "Gravity";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    sensorType = "Gyroscope";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    sensorType = "User_Acceleration";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    sensorType = "Calibrated_Magnetic_Field";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_ROTATION_VECTOR:
                    sensorType = "Attitude";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
            }

            Double magnitude = Math.sqrt((Math.pow(Double.parseDouble(axisX), 2)) +
                    (Math.pow(Double.parseDouble(axisY), 2)) +
                    (Math.pow(Double.parseDouble(axisZ), 2)));

            mWriter.writeNext(new String[]{magnitude.toString()}, false);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public void setUpSensors() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mLinearAccelerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mMagneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mUncalibratedMagneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        mSensorManager.registerListener(mListener, mAccelerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mGyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mLinearAccelerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mMagneticSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mRotationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mUncalibratedMagneticSensor, SensorManager.SENSOR_DELAY_NORMAL);

        prepareFiles();
    }

    public void prepareFiles() {
        File fileDir = (Environment.getExternalStorageDirectory());
        if(radioGroupText1 == null || radioGroupText2 == null || radioGroupText3 == null) {
            mFileToRecord = new File(fileDir, "test.csv");
        } else {
            String[] names = new String[]{radioGroupText1, radioGroupText2, radioGroupText3};
            File templatesDirectory= new File(fileDir + "/motion_templates");
            if (!templatesDirectory.exists()) {
                templatesDirectory.mkdirs();
            }
            String name = "";
            for (int i = 0; i < names.length; ++i) {
                if (!names[i].equals("")) {
                    if (name == "") {
                        name = names[i];
                    } else {
                        name = name + "_" + names[i];
                    }
                }
            }
            mFileToRecord = new File(templatesDirectory, name);
        }

        try {
            if(mFileToRecord.exists()) {
                mFileToRecord.delete();
            }
            mFileToRecord.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (mWriter != null) {
                mWriter.close();
            }
            mWriter = new CSVWriter(new FileWriter(mFileToRecord, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}