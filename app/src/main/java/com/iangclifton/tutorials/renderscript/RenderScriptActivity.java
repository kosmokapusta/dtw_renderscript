package com.iangclifton.tutorials.renderscript;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import au.com.bytecode.opencsv.CSVReader;

public class RenderScriptActivity extends Activity {

    private Button mButtonStart;
    private TextView mTextView;

    private File mFile = new File((Environment.getExternalStorageDirectory()), "test.csv");
    private File mTemplatesDirectory;
    private List<Float> mTest;


    private ProgressDialog mProgress;
    private int mProgressStatus = 0;
    private final Handler mHandler = new Handler();
    RenderScript mRenderScript;

    String mType;

    final Runnable updateRunnable = new Runnable() {
        public void run() {
            updateUI();
        }
    };

    final Runnable finishUpdate = new Runnable() {
        public void run() {
            updateFinishUI();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRenderScript = RenderScript.create( this);

        mButtonStart = (Button) findViewById(R.id.button1);
        mTextView = (TextView) findViewById(R.id.textView);

        mTemplatesDirectory= new File((Environment.getExternalStorageDirectory()) + "/motion_templates");
    }


    public void doStartClick(View view){
        Intent intent = new Intent(this, TrackSensorsService.class);
        startService(intent);
        mProgress = ProgressDialog.show(this, "Recording...",
                "Please, do not stop.", true);

        mButtonStart.setText("Recording");
        mTextView.setText("");

        startProgress();
    }

    public void doResultClick(View view){
        new CompareFilesTask().execute(mFile.getAbsolutePath());
    }

    public void doTemplateClick(View view){
        Intent intent = new Intent(this, RecordActivity.class);
        startActivity(intent);
    }

    public void updateUI() {
        mButtonStart.setText("Done!");
        mButtonStart.setEnabled(false);
        Intent intent = new Intent(this, TrackSensorsService.class);
        stopService(intent);
    }

    public void updateFinishUI() {
        mRenderScript.destroy();
        mButtonStart.setText("Start to record");
        mButtonStart.setEnabled(true);
        if(mType != null) {
            mTextView.setText("Hahaha you are " + (mType+ "ed").toUpperCase() + "!!");
        } else {
            mTextView.setText("You are dancing?");
        }

    }

    public List<Float> readFile(String file) {
        CSVReader reader;
        List<Float> vector = new Vector<Float>();
        try {
            reader = new CSVReader(new FileReader(file));
            String[] nextLine;

            while ((nextLine = reader.readNext()) != null) {
                vector.add(Float.parseFloat(nextLine[0]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return vector;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, TrackSensorsService.class);
        stopService(intent);
    }

    private void startProgress() {
        new Thread(new Runnable() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(), TrackSensorsService.class);
                startService(intent);

                while (mProgressStatus < 500) {
                    try {
                        Thread.sleep(224);
                        mProgressStatus += 2;
                        mProgress.setProgress(mProgressStatus);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopService(intent);
                mProgress.dismiss();
                mProgressStatus = 0;
                mHandler.post(updateRunnable);
            }
        }).start();
    }

    private class CompareFilesTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... files) {
            if (mFile.exists() && mTemplatesDirectory.exists()) {
                mTest = readFile(mFile.getAbsolutePath());
                File[] listOfFiles =  mTemplatesDirectory.listFiles();

                if ( listOfFiles.length == 0) {
                    Toast.makeText(getBaseContext(), "You don't have any templates to calibrate your results! Record something!", Toast.LENGTH_LONG).show();
                    mHandler.post(finishUpdate);
                    return null;
                } else {
                    HashMap<String, Float> dist_map = new HashMap<String, Float>();

                    for(int count=0; count < listOfFiles.length; count++){
                        float distance = getDistance(listOfFiles[count].getAbsolutePath());
                        dist_map.put(listOfFiles[count].getName(), distance);
                    }

                    Float min = Collections.min(dist_map.values());

                    for (String winner_name : dist_map.keySet()) {
                        if (dist_map.get(winner_name).equals(min)) {
                            mType = winner_name;
                        }
                    }
                    mHandler.post(finishUpdate);
                }
            } else {
                Toast.makeText(getBaseContext(), "You don't have any templates to calibrate your results! Record something!", Toast.LENGTH_LONG).show();
                mHandler.post(finishUpdate);
                return null;
            }
            return null;
        }
    }

    public Allocation getAllocation(List<Float> list) {
        float[] array = new float[list.size()];
        int i = 0;

        for (Float f : list) {
            array[i++] = (f != null ? f : Float.NaN);
        }

        Allocation allocation = Allocation.createSized(
                mRenderScript,
                Element.F32(mRenderScript),
                array.length);

        allocation.copyFrom(array);

        return allocation;
    }

    public float getDistance(String example_path){

        List<Float> example = readFile(example_path);

        if (mTest.size() != example.size()) {
            if (mTest.size() > example.size()) {
                List<Float> newTest = mTest.subList(0, example.size());
                mTest = null;
                mTest = newTest;
            } else {
                List<Float> newExample = example.subList(0, mTest.size());
                example = null;
                example = newExample;
            }
        }

        Allocation testAllocation = getAllocation(mTest);
        Allocation standardAllocation = getAllocation(example);

        ScriptC_test script = new ScriptC_test(
                mRenderScript,
                getResources(),
                R.raw.test);

        script.set_s1len(mTest.size());
        script.set_s2len(example.size());

        script.bind_signal1(testAllocation);
        script.bind_signal2(standardAllocation);

        script.invoke_dtw();
        mRenderScript.finish();

        float result[] = new float[mTest.size()];
        testAllocation.copyTo(result);
        float maxc = result[result.length-1];
        Log.d("RSActivity", "maxc: " + maxc);
        int l = Math.max(example.size(), mTest.size());
        maxc /= l;
        Log.d( "RSActivity", "distance  " + maxc);

        return maxc;
    }
}